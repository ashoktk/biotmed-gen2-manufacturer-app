# Version 0.1.56

**Release date**: Dec 5, 2021

## Changes:

- bug fix
# Version 0.1.55

**Release date**: Dec 5, 2021

## Changes:

- upgrading crud lib for fix the error banner view
# Version 0.1.54

**Release date**: Dec 5, 2021

## Changes:

- upgrading libs for a quick bugfix


# Version 0.1.53

**Release date**: Dec 5, 2021

## Changes:

- supporting 'free text search' (upgrade shared libs)

# Version 0.1.52

**Release date**: Dec 5, 2021

## Bug Fixes:

- Change background color of entity avatar in the header.
- remove titles of all icons

# Version 0.1.51

**Release date**: Dec 5, 2021

## Bug Fixes:

- redundant fetch entities removed before CRUD is initialized
 
# Version 0.1.50

**Release date**: Nov 29, 2021

## Bug Fixes:

- updated crud :
  - fix bug -  The Back button does not correspond to design
  
# Version 0.1.48

**Release date**: Dec 1, 2021

## Changes

- SOFT-2457 you can now empty a date/date-time field properly, and it will be saved as empty
- SOFT-2453 fixing table horizontal scroll
- SOFT-2352 positive min max in number or integer now works as intended
- SOFT-2331 autocomplete options now have wrap to show all text

# Version 0.1.47

**Release date**: Nov 30, 2021

## Changes

- updated crud
  - SOFT-2386 - fixed timezone and locale table renderers

# Version 0.1.46

**Release date**: Nov 29, 2021
## Changes

- updated url created for emailConfirmationLandingPage

# Version 0.1.45

**Release date**: Nov 29, 2021

## Bug Fixes:

- updated crud and base-components:
  - numberInput update + validations - can now be cleared and send null to BE

# Version 0.1.44

**Release date**: Nov 25, 2021

## Bug Fixes:

- some temp bug fix to crud

# Version 0.1.43

**Release date**: Nov 25, 2021

## Changes:

- hiding owner organization id when creating an owner admin user for a new organization

# Version 0.1.42

**Release date**: Nov 25, 2021

## Changes:

- when editing and organization, the organization ID will be used in the reference autocomplete (when adding, it will be undefined).
- update libraries crud and settings-sdk (Set vs. Array in writeModes).

# Version 0.0.41

**Release date**: Nov 24, 2021

## Changes

- update libraries base-components and crud.

# Version 0.0.40

**Release date**: Nov 23, 2021

## Changes

- Change order of header tabs.
- Change platform links color.
- update base-components and crud version.

# Version 0.0.39

**Release date**: Nov 22, 2021

## Changes

- Removing ownerAdminUserTemplateId from crudParams

# Version 0.0.38

**Release date**: Nov 22, 2021

## Changes

- Added userOrganization and caregiver cruds

# Version 0.0.37

**Release date**: Nov 21, 2021

## Changes

- Revise the headers of all the apps to look like Google Apps Style

# Version 0.0.36

**Release date**: Nov 18, 2021

## Changes

- Logout

# Version 0.0.35

**Release date**: Nov 16, 2021

## Changes

- Removed User details

# Version 0.0.34

**Release date**: Nov 11, 2021

## Changes

- Updated crud + base-components:
  - added Creation time and Last modified time converted into local timezone
  - converting empty strings on add/Edit to null before sending to BE
  - paragraph size fix
  - asterisk missing on Address and Name fields fixed

# Version 0.0.33

**Release date**: Nov 10, 2021

## Changes

- Added support for auth-pages breaking changes

# Version 0.0.32

**Release date**: Nov 10, 2021

## Changes

- updated shared libs with:
- Updates to ReferenceAutocomplete fields
- Updates to error dictionaries

# Version 0.0.31

**Release date**: Nov 8, 2021

## Changes

- updated crud v

# Version 0.0.30

**Release date**: Nov 8, 2021

## Changes

- upgrading shared libs:
  - reference fields now take into effect 'valid templates to reference'
  - fixed reference field default value
  - Styling updates to snackbar, reference fields

# Version 0.0.29

**Release date**: Nov 8, 2021

## Changes

- fix for snackbar

# Version 0.0.28

**Release date**: Nov 7, 2021

## Changes

- crud library version updated to 0.1.26
  -add/edit new design changes

# Version 0.0.27

**Release date**: Nov 7, 2021

## Changes

- Organizations CRUD added

# Version 0.0.26

**Release date**: Nov 4, 2021

## Changes

- upgraded libs to get translatable error messages

# Version 0.0.25

**Release date**: Nov 1, 2021

## Changes

- Bump crud library to v 0.1.16 fix all renderers and add measurement
- Bump base components to v 0.1.30

# Version 0.0.24

**Release date**: Nov 1, 2021

## Changes

- upgrading 'shared' libs to enable removal of 'status' step from device add/edit modal

# Version 0.0.23

**Release date**: Oct 28, 2021

## Changes

- added error handling and all the renderers

# Version 0.0.22

**Release date**: Oct 28, 2021

## Changes

- added error handling and all the renderers

# Version 0.0.21

**Release date**: Oct 25, 2021

## Changes

- upgrading 'shared' libs to support reference-type fields

# Version 0.0.20

**Release date**: Oct 24, 2021

## Changes

- use crud factory with builder

# Version 0.0.19

**Release date**: Oct 14, 2021

## Changes

- updated top bar name and role style

# Version 0.0.18

**Release date**: Oct 13, 2021

## Changes

- remove disconnect button

# Version 0.0.17

**Release date**: Oct 13, 2021

## Changes

- Entity type translate - bump crud to 0.1.5

# Version 0.0.16

**Release date**: Oct 13, 2021

## Changes

- Add limit to crud component - bump crud lib
- remove disconnect button

# Version 0.0.15

**Release date**: Oct 13, 2021

## Changes

- update auth-pages v

# Version 0.0.14

**Release date**: Oct 13, 2021

## Changes

-top bar modifications

# Version 0.0.13

**Release date**: Oct 06, 2021

## Changes

- fix select and multi select in add/edit
  - bump base-components to 0.1.20
  - bump crud to 0.1.13

# Version 0.0.12

**Release date**: Oct 12, 2021

## Changes

- device crud

# Version 0.0.11

**Release date**: Oct 4, 2021

- Change favicon

# Version 0.0.10

**Release date**: Oct 4, 2021

- Changed local api url to integration (https://api.int.biot-med.com)

## Changes

- change platform link to console app

# Version 0.0.9

**Release date**: Oct 3, 2021

## Changes

- change platform link to console app

# Version 0.0.8

**Release date**: Sep 30, 2021

## Changes

- added platforms popup menu

# Version 0.0.6

**Release date**: Oct 12, 2021

## Changes

- device crud

# Version 0.0.6

**Release date**: Sep 29, 2021

## Changes

-tabs refresh bug fix

# Version 0.0.5

**Release date**: Sep 26, 2021

## Changes

- update SDK api provider
- update auth pages
- update base-components and use css import

# Version 0.0.4

**Release date**: Sep 22, 2021

## Changes

- Aligned to new theme

# Version 0.0.3

**Released by**: Adi Siman Tov **Release date**: Sep 14, 2021

## Changes

- Added device page with mock builder (hardcoded components)
