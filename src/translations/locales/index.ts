import en from './en.json';
import he from './he.json';

const messages = { en, he };

export default messages;
