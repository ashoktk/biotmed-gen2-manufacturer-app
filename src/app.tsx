/**
 * app.js
 *
 * This is the entry file for the application, only setup and boilerplate
 * code.
 */

// Needed for redux-saga es6 generator support
import 'core-js';

// eslint-disable-next-line no-use-before-define
import React from 'react';
import ReactDOM from 'react-dom';
import { Provider } from 'react-redux';
import FontFaceObserver from 'fontfaceobserver';
import { Router } from 'react-router-dom';
import UmsLogic from '@biot/ums-js-logic';
import { initSdkApi } from '@biotmed/sdk-api-provider';
import { crudParams, PortalTypeEnum } from '@biotmed/crud';

import history from 'src/utils/history';
import AppConfig from 'src/config/AppConfig';
import { activateUserUrl } from '@biotmed/auth-pages';
import AppComponent from './routes/Root';
import configureStore from './redux';

import RoutesEnum from './routes/Root/modules/routes';

// Observe loading of Open Sans (to remove open sans, remove the <link> tag in
// the index.html file and this observer)
const openSansObserver = new FontFaceObserver('Open Sans', {});

// When Open Sans is loaded, add a font-family using Open Sans to the body
openSansObserver.load().then(() => {
  document.body.classList.add('fontLoaded');
});

// Create redux store with session state
const initialState = {};
const baseURL = window.location.origin;
// TODO: hard coded 'en !!! this needs to change when possible
// TODO: baseURL and activateUserUrl include "/" and RoutesEnum.AUTH doesn't. Maybe the "/" should be consistent throughout.
const emailConfirmationLandingPage = `${baseURL}/en/${RoutesEnum.AUTH || ''}${activateUserUrl || ''}`;
const organizationEmailConfirmationLandingPage = `${AppConfig.ORGANIZATION_EMAIL_CONFIRMATION_LANDING_PAGE || ''}`;

crudParams.init({
  applicationType: PortalTypeEnum.MANUFACTURER_PORTAL,
  emailConfirmationLandingPage,
  organizationEmailConfirmationLandingPage,
  envName: AppConfig.ENV_NAME || '',
});
const store = configureStore(initialState);

UmsLogic.init({
  umsBaseURL: `${AppConfig.API_URL}/ums`,
  onRefreshTokenFail: () => {
    // TODO: change to store.logout action.
    // eslint-disable-next-line no-console
    console.log('refreshTokenFailed');
  },
  localStorage,
  sessionStorage,
});
initSdkApi({ basePath: `${AppConfig.API_URL}` });

const MOUNT_NODE = document.getElementById('root');

ReactDOM.render(
  <Provider store={store}>
    <Router history={history}>
      <AppComponent />
    </Router>
  </Provider>,
  MOUNT_NODE,
);
// Install ServiceWorker and AppCache in the end since
// it's not most important operation and if main code fails,
// we do not want it installed
if (process.env.NODE_ENV === 'production') {
  // eslint-disable-next-line global-require,import/no-extraneous-dependencies
  require('offline-plugin/runtime').install();
}
