/* eslint no-unused-vars: ["error", {"args": "none"}] */

declare module '@biot/ums-js-logic' {
  export interface LoginResponse {
    userId: string;
    ownerOrganizationId: string;
    accessJwt: {
      token: string;
      expiration: number;
    };
    refreshJwt: {
      token: string;
      expiration: number;
    };
    mfaRequired: boolean;
    passwordResetRequired: boolean;
  }

  export interface LoginRequest {
    username: string;
    password?: string;
    code?: string;
    rememberMe?: boolean;
  }

  export interface LoginWithTokenRequest {
    refreshToken?: string;
  }

  export interface LogoutRequest {
    refreshToken?: string;
  }
  interface Init {
    umsBaseURL: string;
    onRefreshTokenFail: () => void;
    localStorage: any;
    sessionStorage: any;
  }
  export function init(initRequest: Init): void;

  export function login(loginRequest: LoginRequest): LoginResponse;

  export function loginWithToken(loginWithTokenRequest?: LoginWithTokenRequest): LoginResponse;

  export function isLoggedIn(): boolean;

  export function isUserRemembered(): boolean;

  export function getToken(): string;

  export function logout(logoutRequest: LogoutRequest): void;
}
