import { Saga, Task } from 'redux-saga';

declare module 'redux' {
  export interface Store {
    // eslint-disable-next-line no-unused-vars
    runSaga<S extends Saga<any[]>>(saga: S, ...args: Parameters<S>): Task; // provide the types for `store.sagaTask` here
  }
}
