import UmsLogic from '@biot/ums-js-logic';
import ApiService from '@biotmed/api-service';
import { IApiService } from '@biotmed/api-service/dist/interfaces';

import AppConfig from '../config/AppConfig';

// const Resources: { [x: string]: Request } = {
//   getDevices: {
//     url: 'device/v2/devices?searchRequest=',
//     method: 'GET',
//     requireAuth: true,
//   },
// };

/**
 *
|--------------------------------------------------
| The server service - expose interface to the API.
| Every method returns a promise.
|--------------------------------------------------
*/

// const Resources: { [x: string]: Request } = {};

/**
 |--------------------------------------------------
 | The server service - expose interface to the API.
 | Every method returns a promise.
 |--------------------------------------------------
 */

// const generateEncodedSearchRequest = (params: SearchRequestParams) => {
//   const paramsString = JSON.stringify(params);
//   return encodeURI(paramsString);
// };

class BackendService {
  _apiService: IApiService;

  constructor() {
    if (!AppConfig.API_URL) {
      throw new Error('api url is not set. cannot call server');
    }
    this._apiService = new ApiService(AppConfig.API_URL, UmsLogic);
  }

  // getDevices = (params: SearchRequestParams) =>
  //   this._apiService.sendRequest({
  //     ...Resources.getDevices,
  //     url: Resources.getDevices.url + generateEncodedSearchRequest(params),
  //   });
}

export default new BackendService();
