import { standardTheme } from '@biotmed/base-components';

const theme = {
  ...standardTheme,
};

export default theme;
