import {
  OrganizationCrud,
  organizationCrudReducer,
  organizationCrudSaga,
  organizationCrudStateKey,
  organizationCrudGetInitialState,
} from '@biotmed/crud';

export { organizationCrudReducer, organizationCrudSaga, organizationCrudStateKey, organizationCrudGetInitialState };

export default OrganizationCrud;
