import {
  DeviceCrud,
  deviceCrudReducer,
  deviceCrudSaga,
  deviceCrudStateKey,
  deviceCrudGetInitialState,
} from '@biotmed/crud';

export { deviceCrudReducer, deviceCrudSaga, deviceCrudStateKey, deviceCrudGetInitialState };

export default DeviceCrud;
