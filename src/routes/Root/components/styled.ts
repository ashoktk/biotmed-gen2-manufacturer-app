import { CircularProgress } from '@material-ui/core';
import styled from 'styled-components';

export const SpinBox = styled.div`
  height: 100vh;
  display: flex;
  align-items: center;
  justify-content: center;
`;
export const Spin = styled(CircularProgress)`
  color: ${props => props.theme.palette.primary.dark};
`;
