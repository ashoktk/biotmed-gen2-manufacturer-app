// eslint-disable-next-line no-use-before-define
import React from 'react';
import { Switch, Route, Redirect, RouteComponentProps, useRouteMatch, RouteProps } from 'react-router-dom';
import LanguageProvider from 'src/components/LanguageProvider';
import { AuthSwitch, LoginResponse } from '@biotmed/auth-pages';
import AppConfig from 'src/config/AppConfig';
import AppLayout from 'src/components/AppLayout';
import { ReactComponent as LogoIcon } from 'src/images/logo.svg';

import { actions as loginActions } from 'src/redux/data/login/modules/slice';
import { useDispatch, useSelector } from 'react-redux';
import Devices from 'src/routes/Devices';
import Organizations from 'src/routes/Organizations';
import { Snackbar } from '@biotmed/base-components';
import { selectors } from '../../../redux/data/user';
import { MatchParams } from './Root';
import routes from '../modules/routes';

interface MainRoutesProps extends RouteProps {
  isLoggedIn: boolean;
}
const PublicPage: React.FC<MainRoutesProps> = props => {
  const match = useRouteMatch<MatchParams>();
  const dispatch = useDispatch();
  const {
    params: { language },
  } = match;
  const { isLoggedIn } = props;

  const onLoginSuccess = (loginResponse: LoginResponse) => {
    dispatch(loginActions.loginSuccess({ loginResponse }));
  };

  return !isLoggedIn ? (
    <AuthSwitch
      baseUrl={AppConfig.API_URL || ''}
      onLoginSuccess={onLoginSuccess}
      companyDetails={{
        name: 'BioT Manufacturer Portal',
        description: 'Connect. Collaborate. Care.',
        Logo: () => <LogoIcon />,
      }}
      onTermsClick={() => {}}
      onPrivacyClick={() => {}}
    />
  ) : (
    <Redirect to={{ pathname: `/${language}` }} />
  );
};

const PrivatePage: React.FC<MainRoutesProps> = props => {
  const {
    path,
    params: { language },
  } = useRouteMatch<MatchParams>();

  const { isLoggedIn } = props;
  return isLoggedIn ? (
    <AppLayout>
      <Switch>
        <Route exact path={`${path}${routes.DEVICES}`} render={() => <Devices />} />
        <Route exact path={`${path}${routes.ORGANIZATIONS}`} render={() => <Organizations />} />
        <Redirect from={`${path}`} to={`${path}${routes.ORGANIZATIONS}`} />
      </Switch>
    </AppLayout>
  ) : (
    <Redirect to={{ pathname: `/${language}/${routes.AUTH}` }} />
  );
};

const MainRoutes: React.FC<MainRoutesProps> = () => {
  const { path } = useRouteMatch();
  const isLoggedIn = !!useSelector(selectors.getUserId);

  return (
    <LanguageProvider>
      <Snackbar>
        <Switch>
          <Route
            sensitive
            path={`${path}/${routes.AUTH}`}
            component={(p: RouteComponentProps<MatchParams>) => <PublicPage {...p} isLoggedIn={isLoggedIn} />}
          />
          <Route path={`${path}/*`} render={p => <PrivatePage {...p} isLoggedIn={isLoggedIn} />} />
          <Redirect from={`${path}`} to={`${path}/`} />
        </Switch>
      </Snackbar>
    </LanguageProvider>
  );
};

export default MainRoutes;
