// eslint-disable-next-line no-use-before-define
import React, { useEffect } from 'react';
import { Switch, Route, Redirect } from 'react-router-dom';
import { useDispatch, useSelector } from 'react-redux';
import { ThemeProvider } from 'styled-components';
import theme from 'src/themes/theme';
import GlobalStyle from '../../../global.styled';
import { Spin, SpinBox } from './styled';
import { getLanguageRegex, getDefaultLanguage } from '../../../utils/languageUtils';
import MainRoutes from './MainRoutes';
import { selectors as appSelectors, actions as appActions } from '../../../redux/data/app/modules/slice';

export interface MatchParams {
  language: string;
}

interface RootProps {}

const Root: React.FC<RootProps> = () => {
  const isAppLoading = useSelector(appSelectors.getIsAppLoading);
  const dispatch = useDispatch();

  const _onAppStart = () => {
    dispatch(appActions.appStart());
  };

  // eslint-disable-next-line
  useEffect(_onAppStart, []);

  return isAppLoading ? (
    <ThemeProvider theme={theme}>
      <SpinBox>
        <Spin />
      </SpinBox>
    </ThemeProvider>
  ) : (
    <ThemeProvider theme={theme}>
      <GlobalStyle />
      <Switch>
        {/* supported locale */}
        <Route sensitive path={`/:language(${getLanguageRegex()})`} component={MainRoutes} />
        <Redirect from="/*" to={`/${getDefaultLanguage()}`} />
      </Switch>
    </ThemeProvider>
  );
};

Root.defaultProps = {
  isLoggedIn: false,
  isAppLoading: false,
};

export default Root;
