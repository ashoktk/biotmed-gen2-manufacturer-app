/* eslint-disable no-unused-vars */
// eslint-disable-next-line no-shadow
export enum RoutesEnum {
  AUTH = 'auth',
  DEVICES = 'devices',
  ORGANIZATIONS = 'organizations',
}

export default RoutesEnum;
