// eslint-disable-next-line no-use-before-define
import React from 'react';
import { IntlProvider } from 'react-intl';
import { useParams } from 'react-router-dom';
import DateAdapter from '@mui/lab/AdapterDayjs';
import { LocalizationProvider } from '@mui/lab';
import { MatchParams } from '../../../routes/Root/components/Root';
import { getDefaultLanguage, getMessagesByLanguage } from '../../../utils/languageUtils';

interface LanguageProviderProps {
  children: React.ReactNode;
}

export const LanguageProvider: React.FC<LanguageProviderProps> = props => {
  const { children } = props;
  const { language } = useParams<MatchParams>();

  return (
    <IntlProvider
      key={language}
      locale={language}
      defaultLocale={getDefaultLanguage()}
      messages={getMessagesByLanguage(language)}
    >
      <LocalizationProvider dateAdapter={DateAdapter} locale={language}>
        {React.Children.only(children)}
      </LocalizationProvider>
    </IntlProvider>
  );
};

export default LanguageProvider;
