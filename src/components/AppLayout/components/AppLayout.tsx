// eslint-disable-next-line no-use-before-define
import React from 'react';
import { PoweredByBioT, ConnectionAlert } from '@biotmed/base-components';
import { MainContent, Footer, Header, StyledLayout } from './AppLayout.styled';
import TopMenu from './TopMenu';

interface AppLayoutProps {}

const AppLayout: React.FC<AppLayoutProps> = ({ children }) => (
  <StyledLayout>
    <Header>
      <TopMenu />
    </Header>
    <MainContent>
      <ConnectionAlert />
      {children}
    </MainContent>
    <Footer>
      <PoweredByBioT />
    </Footer>
  </StyledLayout>
);

export default AppLayout;
