import React from 'react';
import { Tabs, Icon, EntityAvatar, Button, EntityIconsOptionsEnum } from '@biotmed/base-components';
import routes from 'src/routes/Root/modules/routes';
import history from 'src/utils/history';
import { useDispatch } from 'react-redux';
import { useTheme } from 'styled-components';
import { useIntl } from 'react-intl';
import { actions as loginActions } from '../../../redux/data/login';
import { ReactComponent as OrgIcon } from '../../../images/logo.svg';
import { ReactComponent as LogoIcon } from '../../../images/manufacturerLogo.svg';

import { PlatformsLinksMenu } from './PlatformLinksMenu';
import { LogoContainer, RightContent, TopMenu as TopMenuStyled, User, UserDetails, UserName } from './AppLayout.styled';

interface TopMenuProps {}

const TopMenu: React.FC<TopMenuProps> = () => {
  const intl = useIntl();
  const theme = useTheme();
  const dispatch = useDispatch();
  const currentTab = history?.location?.state as string;

  const handleCallToRouter = (event: React.ChangeEvent<any>, value: string) => {
    history.push(value, value);
  };

  const onDisconnectClick = () => {
    dispatch(loginActions.logout());
  };

  const tabsData = [
    {
      label: intl.formatMessage({
        id: 'app-layout.top-menu.tab.organizations',
        defaultMessage: 'Organizations',
      }),
      tabKey: routes.ORGANIZATIONS,
    },
    {
      label: intl.formatMessage({
        id: 'app-layout.top-menu.tab.devices',
        defaultMessage: 'Devices',
      }),
      tabKey: routes.DEVICES,
    },
  ];
  const rightContent = (
    <RightContent>
      <PlatformsLinksMenu />
      <User>
        <Icon height="18px" IconSVG={OrgIcon} />
        <Button variant="text" onClick={onDisconnectClick} paddingHorizontal="0" paddingVertical="0">
          <EntityAvatar
            type={EntityIconsOptionsEnum.MANUFACTURER_USER}
            size="small"
            backgroundColor={theme.palette.grayScale.light}
            borderColor={theme.palette.grayScale.dark}
          />
        </Button>
        {/* <UserDetails>
          <UserName>Alice Bob</UserName>
        </UserDetails> */}
      </User>
    </RightContent>
  );

  const leftContent = (
    <LogoContainer>
      <Icon height="21" IconSVG={LogoIcon} />
    </LogoContainer>
  );

  return (
    <TopMenuStyled>
      <Tabs
        tabBarExtraContent={{
          left: leftContent,
          right: rightContent,
          spaceBetween: 80,
        }}
        tabsData={tabsData}
        backgroundActive={`${theme.palette.primary.dark}${theme.opacity.almostTransparent}`}
        textColorActive={theme.palette.primary.medium}
        indicatorOverAll
        selectedTabKey={currentTab}
        padding="0 21px"
        onChange={handleCallToRouter}
      />
    </TopMenuStyled>
  );
};
export default TopMenu;
