import { fork, all } from 'redux-saga/effects';
import { saga as authSaga } from '@biotmed/auth-pages';
import { deviceCrudSaga, referenceAutocompleteSaga, measurementsSaga, organizationCrudSaga } from '@biotmed/crud';
import { saga as dataSaga } from '../data';

/*
 * The entry point for all general sagas used in this application.
 */
export default function* root() {
  yield all([
    fork(dataSaga),
    fork(deviceCrudSaga),
    fork(organizationCrudSaga),
    fork(authSaga),
    fork(referenceAutocompleteSaga),
    fork(measurementsSaga),
  ]);
}
