import { all, put, call, takeLatest } from 'redux-saga/effects';
import UmsLogic, { LoginResponse } from '@biot/ums-js-logic';
import { actions as loginActions } from 'src/redux/data/login/modules/slice';
import { actions } from './slice';

function* handleLogin() {
  const isLoggedIn: boolean = yield call(UmsLogic.isLoggedIn);
  const isUserRemembered: boolean = yield call(UmsLogic.isUserRemembered);

  if (!isLoggedIn && !isUserRemembered) {
    return;
  }

  try {
    const response: LoginResponse = yield call(UmsLogic.loginWithToken);
    yield put(loginActions.loginSuccess({ loginResponse: response }));
  } catch (e) {
    // eslint-disable-next-line no-console
    console.log('Error while trying to login with token: ', e);
  }
}

function* onAppStart() {
  yield call(handleLogin);

  yield put(actions.appStartFinish(false));
}

export default function* watchAppActions() {
  yield all([takeLatest(actions.appStart, onAppStart)]);
}
