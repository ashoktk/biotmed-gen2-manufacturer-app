import { createSlice, createSelector } from '@reduxjs/toolkit';
import { actions as loginActions } from '../../login/modules/slice';
import { RootState } from '../../../reducer';
import { DATA_STATE_KEY } from '../../constants';

export const STATE_KEY = 'user';

interface UserState {
  userId?: string | null;
}

/* eslint no-unused-vars: ["error", {"args": "none"}] */
export const getInitialState = (state?: any): UserState => ({
  userId: null,
});

/* eslint-disable no-param-reassign */
const slice = createSlice({
  name: STATE_KEY,
  initialState: getInitialState(),
  reducers: {},
  extraReducers: builder => {
    builder.addCase(loginActions.loginSuccess, (state, action) => {
      const { loginResponse } = action.payload;
      state.userId = loginResponse.userId;
    });
  },
});
/* eslint-enable no-param-reassign */

const getState = (state: RootState) => state[DATA_STATE_KEY][STATE_KEY] || getInitialState();

export const selectors = {
  getUserId: createSelector(getState, state => state.userId),
};

export const { actions } = slice;

const { reducer } = slice;
export default reducer;
