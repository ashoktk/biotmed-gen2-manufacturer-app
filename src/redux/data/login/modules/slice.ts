import { createSlice, createAction } from '@reduxjs/toolkit';
import { LoginResponse } from '@biotmed/auth-pages';

export const STATE_KEY = 'login';

interface LoginState {}

// eslint-disable-next-line no-unused-vars
export const getInitialState = (state?: any): LoginState => ({});

/* eslint-disable no-param-reassign */
const slice = createSlice({
  name: STATE_KEY,
  initialState: getInitialState(),
  reducers: {},
});
/* eslint-enable no-param-reassign */

const extraActions = {
  loginSuccess: createAction<{ loginResponse: LoginResponse }>(`${STATE_KEY}/loginSuccess`), // handled by user reducer
  loginFail: createAction<string>(`${STATE_KEY}/loginFail`),
  logout: createAction(`${STATE_KEY}/logout`),
  logoutFinish: createAction(`${STATE_KEY}/logoutFinish`), // handled by user reducer
};

export const actions = { ...slice.actions, ...extraActions };

export const selectors = {};

const { reducer } = slice;
export default reducer;
