import messages from '../translations/locales';

const SUPPORTED_LANGUAGES = {
  en: 'en',
  he: 'he',
};

const LANGUAGE_MESSAGES: { [x: string]: { [x: string]: string } } = {
  [SUPPORTED_LANGUAGES.en]: messages.en,
  [SUPPORTED_LANGUAGES.he]: messages.he,
};

export const SUPPORTED_LANGUAGE_TEXT_MAPPER = {
  [SUPPORTED_LANGUAGES.en]: 'English',
  [SUPPORTED_LANGUAGES.he]: 'עברית',
};

export const getLanguageRegex: () => string = () => Object.values(SUPPORTED_LANGUAGES).join('|');
export const getDefaultLanguage: () => string = () => SUPPORTED_LANGUAGES.en;
export const getMessagesByLanguage = (lang: string) => LANGUAGE_MESSAGES[lang];
