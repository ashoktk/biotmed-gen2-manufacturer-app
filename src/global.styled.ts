import { createGlobalStyle } from 'styled-components';
import { globalStyles } from '@biotmed/base-components';
import '@biotmed/base-components/base-components.esm.css';

const GlobalStyles = createGlobalStyle`
    ${globalStyles}
`;

export default GlobalStyles;
