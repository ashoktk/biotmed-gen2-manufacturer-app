/* eslint-disable import/no-extraneous-dependencies */
/* eslint-disable no-console */
console.log('*********** react-app-rewired config loading ***********');
/* eslint-enable no-console */

const {
  addBabelPlugins,
  addDecoratorsLegacy,
  addWebpackAlias,
  addWebpackPlugin,
  fixBabelImports,
  override,
  overrideDevServer,
  watchAll,
} = require('customize-cra');
const path = require('path');
const ProgressBarPlugin = require('progress-bar-webpack-plugin');
const AppConfig = require('./src/config/AppConfig');

module.exports = {
  webpack: override(
    // usual webpack plugin
    addDecoratorsLegacy(),

    // aliases to comply with react-boilerplate imports
    addWebpackAlias({
      src: path.resolve(__dirname, 'src'),
    }),

    // ant design: minify css bundle size
    fixBabelImports('import', {
      libraryName: 'antd',
      libraryDirectory: 'es',
      style: true,
    }),

    ...addBabelPlugins('babel-plugin-styled-components', [
      'formatjs',
      {
        idInterpolationPattern: '[sha512:contenthash:base64:6]',
        ast: true,
      },
    ]),
    addWebpackPlugin(new ProgressBarPlugin()),
    !AppConfig.IS_CONSOLE_LOG_ENABLED && addBabelPlugins('transform-remove-console'),
  ),
  devServer: overrideDevServer(watchAll() /* dev server plugin */),
};
